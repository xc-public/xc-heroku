const {XcConfigFactory, XcApis} = require("xc-instant");
const express = require('express');
const cors = require('cors');

const url = XcConfigFactory.extractXcUrlFromJdbc(process.env.DATABASE_URL);

process.env.XC_DB = url;


(async () => {
    const server = express();
    server.use(cors());
    server.set('view engine', 'ejs');
    const app = new XcApis();
    server.use(await app.init({
      async afterMetaMigrationInit() {
        if (!(await app.xcMeta.projectList()).length) {
          const config = XcConfigFactory.makeProjectConfigFromUrl(url);
          const project = await app.xcMeta.projectCreate(config.title, config, '');
          await app.xcMeta.projectStatusUpdate(config.title, 'started');
          await app.xcMeta.projectAddUser(project.id, 1);
        }
      }
    }));
    server.listen(process.env.PORT || 8080, () => {
      console.log(`App started successfully.\nVisit -> http://localhost:${process.env.PORT || 8080}/xc`);
    })
  }
)().catch(e => console.log(e))
