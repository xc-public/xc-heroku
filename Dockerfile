FROM alpine:3.12

ENV PORT 8080
ENV NODE_ENV=dev
#ENV TOOL_DIR=/tool
ENV XC_VERSION=0.6
ENV XC=xpranav

# Create and change to the app directory.
WORKDIR /usr/src/app

# Copy application dependency manifests to the container image.
# A wildcard is used to ensure both package.json AND package-lock.json are copied.
# Copying this separately prevents re-running npm install on every code change.
COPY ./index.js ./
COPY ./package*.json ./

# Install production dependencies.
RUN apk --update --no-cache add \
    	nodejs \
    	nodejs-npm \
    	tar\
   && npm install --cache=/usr/src/app/cache --production \
    && npx modclean --patterns="default:*" --ignore="xc-lib-gui/**,dayjs/**,express-status-monitor/**" --run  \
    && rm -rf ./node_modules/sqlite3/deps \
    && rm -rf ./node_modules/xc-lib-gui/lib/dist/_nuxt \
    && rm -rf /usr/src/app/cache && rm -rf /root/.npm \
   && apk del nodejs-npm \

# Run the web service on container startup.
CMD [ "node", "index.js" ]



